<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Otomanopee+One&display=swap" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-3.6.0.min.js"></script>
    <title>product</title>
</head>
<body>
<?php include('header.php'); ?>


<section id="job" class="section2">
    <div class="upper-div py-2">
        
        <div class=" px-4 px-lg-5 mt-5">
            
                        <div class="container dropdown-container">
                        <h2>Looking for a job?</h2>
                            <div class="dropdown mb-3 drop">
                            <button class="btn btn-secondary dropdown-toggle"
                             type="button" id="dropdownMenuButton" data-toggle="dropdown"
                              aria-haspopup="true" aria-expanded="false">
                              Industry
                            </button>
                            </div>
                            <div class="dropdown mb-3 drop">
                            <button class="btn btn-secondary dropdown-toggle"
                             type="button" id="dropdownMenuButton" data-toggle="dropdown"
                              aria-haspopup="true" aria-expanded="false">
                              location
                            </button>
                            
                            </div>
                            <button type="submit" class="btn btn-primary mb-3 submit">Cast Me</button>
                         </div>
                         <div class="container btn-container ">
                         <button type="submit" class="btn btn-primary">Movies</button>
                         <button type="submit" class="btn btn-primary">Short Movies</button>
                         <button type="submit" class="btn btn-primary">University Project</button>
                         <button type="submit" class="btn btn-primary">Voice Over</button>
                         <button type="submit" class="btn btn-primary">Beirut</button>
                         <button type="submit" class="btn btn-primary">Dubai</button>
                         <button type="submit" class="btn btn-primary">Tap Dancing</button>
                         </div>
        </div>
    </div>



    <div class="py-3 bg-light bottom-div">
                <div class="container px-4 px-lg-5 mt-5">
                    <h2 class="fw-bolder mb-4">Related products</h2>
                    <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                        <div class="col mb-5">
                            <div class="card h-100">
                                <!-- Product image-->
                                <div class="badge bg-dark text-white position-absolute" style="top: 0.5rem; right: 0.5rem">Movie Name</div>
                                <img class="card-img-top" src="/assets/img/Cast-Me.png" alt="..." />
                                
                                <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#">Talent required 1 role</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col mb-5">
                            <div class="card h-100">
                                <!-- Sale badge-->
                                <div class="badge bg-dark text-white position-absolute" style="top: 0.5rem; right: 0.5rem">Movie Name</div>
                                <!-- Product image-->
                                <img class="card-img-top" src="/assets/img/Cast-Me.png" alt="..." />
                               
                                <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#">Talent required 1 role</a></div>
                                </div>
                            </div>
                        </div>
                        <div class="col mb-5 d-sm-none d-lg-block">
                            <div class="card h-100">
                                <!-- Sale badge-->
                                <div class="badge bg-dark text-white position-absolute" style="top: 0.5rem; right: 0.5rem">Movie Name</div>
                                <!-- Product image-->
                                <img class="card-img-top" src="/assets/img/Cast-Me.png" alt="..." />
                                
                                <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                                    <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="#">Talent required 1 role</a></div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
    </div>
</section>






<section id="talents" class="section3">


    <div class="container">

    <h2 class="">talents</h2>


    <div class="row text-center text-lg-left">

        <div class="col-lg-3 col-md-4 col-6">
        <a href="#" class="d-block mb-4 h-100">
                <div class="item red">
                <img src="/assets/img/talents-featured.png"/>
                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                </div>  
                <h3>one line description 32 words maximum</h3>
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6">
        <a href="#" class="d-block mb-4 h-100">
                <div class="item red">
                <img src="/assets/img/talents-featured.png"/>
                <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                </div>
                <h3>one line description 32 words maximum</h3>
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6">
        <a href="#" class="d-block mb-4 h-100">
                <div class="item red">
                <img src="/assets/img/talents-featured.png"/>
                <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                </div>
                <h3>one line description 32 words maximum</h3>
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-6">
        <a href="#" class="d-block mb-4 h-100">
                <div class="item red">
                    <img src="/assets/img/talents-featured.png"/>
                    <i class="fa fa-play-circle-o" aria-hidden="true"></i>
                </div>
                <h3>one line description 32 words maximum</h3>
            </a>
        </div>
    
    </div>

</section>


    <?php include('footer.php'); ?>
</body>
</html>