<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="assets/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/main.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Otomanopee+One&display=swap" rel="stylesheet">
    <script type="text/javascript" src="/assets/js/jquery/jquery-3.6.0.min.js"></script>
    <title>product</title>
</head>
<body>
  
<section id="topbar" class="topbar d-none d-lg-block">
    <div class="d-flex flex-row-reverse">
      <div class="contact-info">
        <ul>
          <li class="p-2"><a href="mailto:contact@example.com">join</a></li>
          <li class="p-2"><i> start a job</i></li>
          <li class="p-2"><i>hello </i></li>
        </ul>

      </div>

    </div>
  </section>


  <nav class="navbar navbar-expand-lg navbar-light">
     <div class="container logo d-flex">
        <a href="index.php"><img src="/assets/img/logo.png" alt="" class="img-fluid"></a>
      </div> 
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class=" collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-lg-auto">
            <li class="nav-item">
                <a class="nav-link" href="#job">jobs <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#talents">talent</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#props">props</a>
            </li>          
            </ul>
        </div>
</nav>




<section class="header py-5">
  <div class="divider"></div>
    <header class="px-4 px-lg-5 my-5">
      <div class="row gx-4 gx-lg-5 align-items">
        <div class="col-md-1"></div>
          <div class=" col-md-12 col-lg-6 col-xl-3">

          <form>
              <div class="mb-3">

                <input type="email" placeholder="Email Adress" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">

              </div>
              <div class="mb-3">

                <input type="password" placeholder="Password" class="form-control" id="exampleInputPassword1">
              </div>

              <button type="submit" class="btn btn-primary">Login</button>
              <a href="#">Did you forget your password?!</a>
              <button type="submit" class="btn btn-success">Sign up</button>
          </form>

          </div>

          <div class="col-xs-5 col-lg-5 d-flex">
            <h1 class="display-5 fw-bolder">You are the next star!</h1>
          </div>

      </div>

    </header>

</section>




        
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</html>